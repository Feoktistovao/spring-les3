package ru.feoktistov.les3.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;


@Entity
public class Person  {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private String firstName;

    private String lastName;

    @OneToMany(fetch = FetchType.EAGER)
    private List<Person> friends;

    public long getId() {
        return id;
    }


    public List<Person> getFriends() {
        return friends;
    }

    public void setFriends(List<Person> friends) {
        this.friends = friends;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public String toString() {
        return "id: " + this.id + " FistName: " + this.firstName + " LastName: " + this.lastName + "  ListFriends " + this.friends ;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }



}
