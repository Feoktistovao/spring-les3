package ru.feoktistov.les3;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import ru.feoktistov.les3.entity.Person;
import ru.feoktistov.les3.repository.PersonRepository;

import java.util.Collection;
import java.util.Collections;

@SpringBootApplication
public class Les3Application {
	public static void main(String[] args) {
		SpringApplication.run(Les3Application.class, args);

	}

	@Bean
	public CommandLineRunner demo(PersonRepository repository) {
		return (args) -> {
			// save a few customers
			Person person = new Person();
			person.setLastName("Alex");
			person.setFirstName("Bolton");
			Person savePerson1 = repository.save(person);

			Person person2 = new Person();
			person2.setFirstName("Oleg");
			person2.setLastName("Nikolaev");
			Person savePerson2 = repository.save(person2);

			Person person3 = new Person();
			person3.setFirstName("Daria");
			person3.setLastName("Novikova");
			Person savePerson3 = repository.save(person3);

			person.setFriends(Collections.singletonList(savePerson2));

			repository.save(person);


			for (Person person1 : repository.findAll()) {
				System.out.println(person1.toString());
				/*System.out.println(person1.getFriends());*/
			}

		};
	}
}
