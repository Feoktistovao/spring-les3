package ru.feoktistov.les3.repository;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import ru.feoktistov.les3.entity.Person;

import java.util.List;


public interface PersonRepository extends CrudRepository<Person, Long> {

    List<Person> findByFirstName(String firstName);

    List<Person> findByLastName(String lastName);

    List<Person> findByFriends(Person friends);

    Person findById(long id);

}
